# Fake Log Generator Microservice

This microservice is a simple Go application designed to generate and return fake log messages. It's useful for testing, development purposes, and when you need to simulate log data without generating actual events in your systems. The service exposes a REST API endpoint, returning a randomly generated log entry with a timestamp, a log level, and a message upon each request.

## Getting Started

### Prerequisites

- Go (version 1.20 or later recommended)

### Installation

1. **Clone the repository to your local machine:**

   ```sh
   git clone https://github.com/yourgithubusername/fake-log-generator.git
2. **Navigate into the project directory:**
    ```sh
    cd fake-log-generator
3. **There's no need to install dependencies manually.**
Go Modules will automatically handle them when you build or run the service.

### Running the Service
To run the microservice, execute the following command in the terminal at the project's root directory:

```sh
go run main.go
```
This command starts the server on port 8080 (make sure the port is available).

## Usage
With the service running, you can generate a fake log by making a GET request to the following URL:

```sh
http://localhost:8080/generate-log
```
You can use tools like curl, Postman, or your browser to make the request.

Example using curl:
```sh
curl http://localhost:8080/generate-log
```
### Response example:

```json
{
  "timestamp": "2024-03-06T15:04:05Z",
  "level": "INFO",
  "message": "Action completed successfully."
}
```
The response will include a JSON object with a timestamp, level, and message fields, representing the fake log entry.

### Features
Simple REST API: A single endpoint to generate and retrieve a fake log entry.
Random Log Generation: Each log entry contains randomly selected log levels and messages to simulate a variety of log types.