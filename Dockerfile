FROM golang:1.22 AS builder

WORKDIR /app

COPY go.* ./
RUN go mod download

COPY . ./

RUN CGO_ENABLED=0 GOOS=linux go build -v -o fake-logs-service 2>&1 | tee build.log

FROM scratch AS runtime

COPY --from=builder /app/fake-logs-service /fake-logs-service

CMD ["/fake-logs-service"]