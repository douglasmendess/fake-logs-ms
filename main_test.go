package main

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestHandleGenerateLog(t *testing.T) {
	req, err := http.NewRequest("GET", "/generate-log", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(handleGenerateLog)

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusOK)
	}

	var log Log
	if err := json.NewDecoder(rr.Body).Decode(&log); err != nil {
		t.Fatal("Expected JSON response, got error:", err)
	}

}
