package main

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"net/http"
	"time"
)

type Log struct {
	Timestamp string `json:"timestamp"`
	Level     string `json:"level"`
	Message   string `json:"message"`
}

func generateFakeLog() Log {
	levels := []string{"INFO", "WARN", "ERROR", "DEBUG"}
	messages := []string{"Action completed successfully", "System alert", "Error processing request", "Debug data"}

	return Log{
		Timestamp: time.Now().Format(time.RFC3339),
		Level:     levels[rand.Intn(len(levels))],
		Message:   messages[rand.Intn(len(messages))],
	}

}

func handleGenerateLog(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "Method not supported", http.StatusMethodNotAllowed)
		return
	}

	log := generateFakeLog()

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(log)
}

func main() {

	http.HandleFunc("/generate-log", handleGenerateLog)
	fmt.Println("Server started on port 8080!")
	http.ListenAndServe(":8080", nil)
}
